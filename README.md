# Misaki's Website

The website `https://misakirschtorte.gitlab.io/website/`


## Setup

```
TODO
```


## License

Copyright (c) 2017 Misaki Asaka

### Software

[BSD 3-Clause LICENSE](https://opensource.org/licenses/BSD-3-Clause)

### Articles, Site images (content)

[![Creative Commons License](
https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](
http://creativecommons.org/licenses/by-nc-sa/4.0/)

This work is licensed under a [
Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International
License](https://creativecommons.org/licenses/by-nc-sa/4.0/).

See `LICENSE` or check out [Legalcode](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode)
